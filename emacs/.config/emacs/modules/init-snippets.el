;;; init-snippets.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-snippets.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Abbrevs
(use-package abbrev
  :straight (:type built-in)
  :init
  (setq save-abbrevs 'silently)
  (setq abbrev-file-name (locate-user-emacs-file "my-abbrevs.el"))
  :config
  (setq-default abbrev-mode t)
  (setq only-global-abbrevs nil))

;; Auto-inserts
(use-package autoinsert
  :straight (:type built-in)
  :init
  (setq auto-insert-query nil)
  (setq auto-insert-directory (locate-user-emacs-file "templates"))
  (auto-insert-mode 1)
  :config
  (define-auto-insert "dev-requirements.in" "dev-requirements.in")
  (define-auto-insert "\\.\\org" "org-insert.org"))

(provide 'init-snippets)
;;; init-snippets ends here
