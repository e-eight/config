;;; init-R.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/modules/init-R.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(use-package ess-site
  :straight ess
  :commands R
  :config
  ;; disable ido
  (setq ess-use-ido nil)
  ;; "asynchronous" R evaluation
  (setq ess-eval-visibly 'nowait)
  ;; unbind `ess-insert-asign' (default value is "_")
  (setq ess-smart-S-assign-key nil)
  ;; RStudio coding style
  (setq ess-style 'RStudio)
  :hook
  (ess-post-run . ess-execute-screen-options)
  (ess-mode . subword-mode))

(use-package ess-r-mode
  :straight ess
  :config
  ;; Hot key C-S-m for pipe operator in ESS
  (defun pipe_R_operator ()
    "R - %>% operator or 'then' pipe operator"
    (interactive)
    (just-one-space 1)
    (insert "%>%")
    (just-one-space 1))

  ;; ESS syntax highlight
  (setq ess-R-font-lock-keywords
        '((ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:constants . t)
          (ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . t)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)
          (ess-R-fl-keyword:%op% . t)))

  (setq inferior-ess-r-font-lock-keywords
        '((ess-S-fl-keyword:prompt . t)
          (ess-R-fl-keyword:messages . t)
          (ess-R-fl-keyword:modifiers . nil)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . nil)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:matrix-labels . t)
          (ess-fl-keyword:fun-calls . nil)
          (ess-fl-keyword:numbers . nil)
          (ess-fl-keyword:operators . nil)
          (ess-fl-keyword:delimiters . nil)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . nil)))

  :bind (:map ess-r-mode-map
              ("M--" . ess-insert-assign)
              ("C-S-m" . pipe_R_operator)
              :map inferior-ess-r-mode-map
              ("M--" . ess-insert-assign)
              ("C-S-m" . pipe_R_operator)))

(provide 'init-R)
;;; init-R.el ends here
