;;; init-ui.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-ui.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Use the packaged version of the modus themes:
;; -operandi (light) and -vivendi (dark)
(use-package modus-themes
  :init (modus-themes-load-themes)
  :config (modus-themes-load-operandi)
  :bind ("<f5>" . modus-themes-toggle))

;; Use the wombat theme
;; (use-package emacs
;;   :config
;;   (load-theme 'wombat t))

;; Show batter status when Emacs is running on laptop
(use-package battery
  :straight (:type built-in)
  :config
  (setq battery-mode-line-format "%b%p%%%, remaining time: %t")
  (setq battery-mode-line-limit 95)
  (setq battery-update-interval 180)
  (setq battery-load-low 20)
  (setq battery-load-critical 10)
  :hook (after-init . display-battery-mode))


;; Simplify information displayed on modeline
;; Adapted from https://emacs-fu.blogspot.com/2011/08/customizing-mode-line.html
(setq-default mode-line-format
              (list
               ;; version control
               '(:eval (propertize (substring vc-mode 0) 'face 'font-lock-comment-face))
               ;; the buffer name; the file name as a tool tip
               '(:eval (propertize " %b " 'face 'font-lock-keyword-face
                                   'help-echo (buffer-file-name)))
               ;; line and column
               "(" ;; '%02' to set to 2 chars at least; prevents flickering
               (propertize "%02l" 'face 'font-lock-type-face) ","
               (propertize "%02c" 'face 'font-lock-type-face)
               ") "
               ;; position in file, file size
               "["
               (propertize "%o" 'face 'font-lock-constant-face) ;; % above top
               "/"
               (propertize "%I" 'face 'font-lock-constant-face) ;; size
               "] "
               "[" ;; insert vs overwrite mode, input-method in a tooltip
               '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
                                   'face 'font-lock-preprocessor-face
                                   'help-echo (concat "Buffer is in "
                                                      (if overwrite-mode "overwrite" "insert") " mode")))

               ;; was this buffer modified since the last save?
               '(:eval (when (buffer-modified-p)
                         (concat ","  (propertize "Mod"
                                                  'face 'font-lock-warning-face
                                                  'help-echo "Buffer has been modified"))))

               ;; is this buffer read-only?
               '(:eval (when buffer-read-only
                         (concat ","  (propertize "RO"
                                                  'face 'font-lock-type-face
                                                  'help-echo "Buffer is read-only"))))
               "] "
               ;; padding for right align
               '(:eval (propertize
                        " " 'display
                        `((space :align-to (- (+ right right-fringe right-margin)
                                              ,(+ 3
                                                  (if (eq major-mode 'emacs-lisp-mode)
                                                      (string-width "ELisp")
                                                    (string-width mode-name))
                                                  (string-width battery-mode-line-string)))))))
               ;; the current major mode of the buffer
               '(:eval (let ((current-mode-name))
                         (if (eq major-mode 'emacs-lisp-mode)
                             (setq current-mode-name "ELisp")
                           (setq current-mode-name mode-name))
                         (propertize current-mode-name 'face 'font-lock-string-face
                                     'help-echo (format "%s" buffer-file-coding-system))))
               ;; battery info
               '(:eval (format " %s"
                               (propertize battery-mode-line-string 'face
                                           'font-lock-warning-face)))))


(provide 'init-ui)
;;; init-ui ends here
