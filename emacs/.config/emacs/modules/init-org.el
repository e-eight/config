;;; init-org.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-org.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Install `org-contrib'
(use-package org-contrib
  :defer t)

;; Install `org'
(use-package org
  :defer 2
  :config
  (setq org-latex-pdf-process '("latexmk -pdf -outdir=%o %f"))
  (setq org-export-with-smart-quotes t)

  ;; export citations
  (require 'ol-bibtex)

  ;; manage citations
  (require 'org-bibtex-extras)

  ;; ignore headline but include content when exporting
  (require 'ox-extra)
  (ox-extras-activate '(ignore-headlines))

  ;; `org-tempo' for old style org template expansion
  (require 'org-tempo)

  ;; simple org-mode skeleton abbrevs
  (define-skeleton org-inline-math-skeleton
    "Skeleton for `org-mode' inline math." "" "\\(" _ "\\)")
  (define-abbrev org-mode-abbrev-table "im" "" 'org-inline-math-skeleton)

  (define-skeleton org-latex-align-skeleton
    "Skeleton for align environment in `org-mode'." ""
    "\\begin{align}\n"_"\n\\end{align}")
  (define-abbrev org-mode-abbrev-table "align" "" 'org-latex-align-skeleton)

  (define-skeleton org-latex-align-star-skeleton
    "Skeleton for align environment in `org-mode'." ""
    "\\begin{align*}\n"_"\n\\end{align*}")
  (define-abbrev org-mode-abbrev-table "aligns" "" 'org-latex-align-star-skeleton)

  (define-skeleton org-italics-skeleton
    "Skeleton for `org-mode' italics." "" "/"_"/")
  (define-abbrev org-mode-abbrev-table "itl" "" 'org-italics-skeleton)

  (define-skeleton org-inline-code-skeleton
    "Skeleton for `org-mode' inline code." "" "="_"=")
  (define-abbrev org-mode-abbrev-table "ic" "" 'org-inline-code-skeleton)

  (define-skeleton org-jupyter-property-skeleton
    "Skeleton for `org-mode' `PROPERTY' header for jupyter source blocks." ""
    "#+PROPERTY: header-args :tangle yes :jupyter-python :session py :kernel "_" :async yes :exports both :eval no-export")
  (define-abbrev org-mode-abbrev-table "jp" "" 'org-jupyter-property-skeleton)


  :custom (org-startup-indented t)
  :bind (:map org-mode-map
              ("<f12>" . org-bibtex-yank)
              ("C-c C-x ]" . insert-org-cite)
              ("C-c !" . org-time-stamp-inactive)))

;; keyboard macro to convert reftex citations to `cite:' links
(defun insert-org-cite ()
  (interactive)
  (org-reftex-citation)
  (setq last-command-event 127)
  (org-delete-backward-char 1)
  (setq last-command-event 93)
  (org-self-insert-command 1)
  (setq last-command-event 93)
  (org-self-insert-command 1)
  (setq last-command-event 134217826)
  (backward-word 1)
  (setq last-command-event 127)
  (org-delete-backward-char 1)
  (setq last-command-event 58)
  (org-self-insert-command 1)
  (setq last-command-event 134217826)
  (backward-word 1)
  (setq last-command-event 127)
  (org-delete-backward-char 1)
  (setq last-command-event 91)
  (org-self-insert-command 1)
  (setq last-command-event 91)
  (org-self-insert-command 1)
  (setq last-command-event 'f4))

;; TOC in org-mode documents
(use-package toc-org
  :after ox
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

;; `org-fragtog' for automatic toggling of `org-latex-preview'
(use-package org-fragtog
  :after ox)

;; `ox-hugo' for blogging
;; Need to define some skeletons for this
(use-package ox-hugo
  :after ox)

;; `org-reveal'(`ox-reveal') for html presentations
(use-package ox-reveal
  :after ox)

;; Jupyter kernel
(use-package jupyter
  :defer 2)

;; Export org-mode to jupyter notebook
(use-package ox-ipynb
  :straight (:host github :repo "jkitchin/ox-ipynb")
  :after ox)

;; Languages for org-babel
;; Following https://blog.d46.us/advanced-emacs-startup/ for faster startup

;; shell
(use-package ob-shell
  :defer 2
  :straight (:type built-in)
  :commands
  (org-babel-execute:sh
   org-babel-expand-body:sh
   org-babel-execute:bash
   org-babel-expand-body:bash))

;; ditaa
(use-package ob-ditaa
  :defer 2
  :straight (:type built-in)
  :commands (org-babel-execute:ditaa))

;; elisp
(use-package ob-emacs-lisp
  :defer 2
  :straight (:type built-in)
  :commands
  (org-babel-execute:elisp
   org-babel-expand-body:elisp
   org-babel-expand-body:emacs-lisp))

;; python
(use-package ob-python
  :defer 2
  :straight (:type built-in)
  :commands (org-babel-execute:python))

;; R
(use-package ob-R
  :defer 2
  :straight (:type built-in)
  :commands
  (org-babel-execute:R
   org-babel-expand-body:R))

;; jupyter
(use-package ob-jupyter
  :defer 2
  :straight (:type built-in)
  :commands
  (org-babel-execute:jupyter
   org-babel-expand-body:jupyter
   org-babel-execute:jupyter-python
   org-babel-expand-body:jupyter-python))


(provide 'init-org)
;;; init-org ends here
