;;; init-defaults.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-defaults.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; No startup screen
(setq inhibit-startup-screen t)

;; No startup message
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message user-login-name)

;; No "For information about GNU Emacs...." at startup
(unless (daemonp)
  (advice-add #'display-startup-echo-area-message :override #'ignore))

;; Pixel scroll as opposed to character scroll
;; (pixel-scroll-mode t)

;; Always start with *scratch*
(setq initial-buffer-choice t)

;; No message in scratch buffer
(setq initial-scratch-message nil)

;; Set scratch buffer to fundamental mode for faster startup
(setq initial-major-mode 'fundamental-mode)

;; No file dialog
(setq use-file-dialog nil)

;; No dialog box
(setq use-dialog-box nil)

;; Disable tooltips
(tooltip-mode -1)
(when (eq system-type 'gnu/linux)
  (setq x-gtk-use-system-tooltips nil))

;; No popup windows
;; (setq pop-up-windows nil)  ;;; not sure if I want this or not

;; Disable audio notifications
(setq ring-bell-function 'ignore)

;; Short answers: y/n instead of yes/no
(if (version< emacs-version "28.0.50")
		(defalias 'yes-or-no-p 'y-or-no-p)
	(setq use-short-answers t))

;; Show column number
(setq column-number-mode t)

;; Always so buffer name in titlebar
(setq frame-title-format "%b")

;; No tabs
(setq-default indent-tabs-mode nil)

;; Tab space equivalence
(setq-default tab-width 2)

;; Size of temporary buffers
(temp-buffer-resize-mode)
(setq temp-buffer-max-height 8)

;; Minimum window height
(setq window-min-height 1)

;; Replace selected text with typed text
(delete-selection-mode 1)

;; Buffer encoding
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment   'utf-8)

;; Save all backups in one location with version control
(defvar emacs-backup-dir (locate-user-emacs-file "backup"))
;; `locate-user-emacs-file' should create the directory if it does not exist
;; (unless (file-exists-p emacs-backup-dir)
;;   (make-directory emacs-backup-dir))
(setq backup-directory-alist `(("." . ,emacs-backup-dir)))
(setq backup-by-copying t)
(setq version-control t)
(setq delete-old-versions t)
(setq kept-new-versions 5)
(setq kept-old-versions 2)
(setq create-lockfiles nil)


(provide 'init-defaults)
;;; init-defaults ends here
