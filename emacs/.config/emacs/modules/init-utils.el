;;; init-utils.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-utils.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; `system-packages' for installing system packages
(use-package system-packages
  :defer 2
  :config
  (setq system-packages-use-sudo t)
  (setq system-packages-package-manager 'xbps-install))

;; `pdf-tools' for reading pdf
(use-package pdf-tools
  :defer 2
  :config
  (setq pdf-tools-enabled-modes
        '(pdf-history-minor-mode
          pdf-isearch-minor-mode
          pdf-links-minor-mode
          pdf-outline-minor-mode
          pdf-misc-size-indication-minor-mode
          pdf-occur-global-minor-mode))
  (setq pdf-view-display-size 'fit-width)
  (setq pdf-view-continuous t)
  (setq pdf-view-use-dedicated-register nil)
  (setq pdf-view-max-image-width 1080)
  (setq pdf-outline-imenu-use-flat-menus t)
  (pdf-loader-install))

;; Ensure that the environment variables inside Emacs are the same as in the
;; shell
(use-package exec-path-from-shell
  :config
  (exec-path-from-shell-initialize))

;; Recent files menu
(use-package recentf
  :straight (:type built-in)
  :defer 2
  :hook (after-init . recentf-mode)
  :init
  (setq recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  (setq recentf-save-file (locate-user-emacs-file "recentf"))
  :config
  (let (save-silently)
    (recentf-save-list))
  (setq recentf-auto-cleanup 'never))

;; Save history between Emacs restarts
(use-package savehist
  :straight (:type built-in)
  :defer 2
  :init
  (setq savehist-file (locate-user-emacs-file "savehist"))
  (setq history-length 10000)
  (setq history-delete-duplicates t)
  (setq savehist-save-minibuffer-history t)
  :hook (after-init . savehist-mode))

;; Automatically update buffer when file changes
(use-package autorevert
  :straight (:type built-in)
  :init (setq auto-revert-verbose nil)
  :hook (after-init . global-auto-revert-mode))

;; Add time-stamp upon save
;; https://protesilaos.com/dotemacs/#h:24c2ac42-a537-4658-8a08-9aebb0de1a73
(use-package time-stamp
  :straight (:type built-in)
  :hook (before-save . time-stamp))

;; `elmacro' for showing keyboard macros as Emacs lisp
(use-package elmacro
  :hook ((prog-mode text-mode) . elmacro-mode))

;; Integration with powerthesaurus.org
(use-package powerthesaurus
  :defer t)

;; LanguageTool for "intelligent" writing assistant
(use-package langtool
  :commands (langtool-check)
  :config
  (setq langtool-language-tool-server-jar "/usr/share/java/languagetool/languagetool-server.jar")
  (setq langtool-default-language "en-US"))

;; Emojis in Emacs
(use-package emojify
  :hook (after-init . global-emojify-mode))

(provide 'init-utils)
;;; init-utils ends here
