;;; init-editor.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-editor.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Use electric-pair mode
(use-package elec-pair
  :straight (:type built-in)
  :hook ((prog-mode text-mode) . electric-pair-mode))

;; Highlight paren pair when the curson on one of the parens
(use-package paren
  :straight (:type built-in)
  :defer 2
  :init (show-paren-mode 1))

;; Set the fill-column to 80 and activate for prog-mode
(use-package emacs
  :straight (:type built-in)
  :config (setq-default fill-column 80)
  :hook (prog-mode . auto-fill-mode))

;; Set visual-line-mode to be used in text buffers
(use-package simple
  :straight (:type built-in)
  :hook (text-mode . visual-line-mode))

;; Wrap visual lines at fill column instead of screen edge
(use-package visual-fill-column
  :defer t
  :custom (advice-add 'text-scale-adjust :after #'visual-fill-column-adjust)
  ;; :hook (visual-line-mode . visual-fill-column-mode))
  :bind ("C-c v f" . visual-fill-column-mode))

;; Show indent guides, particularly useful for python
(use-package highlight-indent-guides
  :hook ((prog-mode text-mode conf-mode) . highlight-indent-guides-mode)
  :init
  (setq highlight-indent-guides-method 'bitmap)
  (setq highlight-indent-guides-suppress-auto-error t))

;; Enable subword-mode in prog-mode
(use-package subword
  :straight (:type built-in)
  :hook (prog-mode . subword-mode))

;; Enable outline-minor-mode in prog-mode and text-mode
(use-package outline
  :straight (:type built-in)
  :hook ((prog-mode text-mode) . outline-minor-mode))

;; Enhance outline-minor-mode with faces from outline-mode
(use-package outline-minor-faces
  :after outline
  :hook (outline-minor-mode . outline-minor-faces-add-font-lock-keywords))

;; Cycle outlines easily
(use-package bicycle
  :after outline
  :bind (:map outline-minor-mode-map
              ([C-tab] . bicycle-cycle)
              ([S-tab] . bicycle-cycle-global)))

;; Use direnv to load local environment
(use-package envrc
  :demand t
  :hook (after-init . envrc-global-mode))

;; temporary hack to fix `peculiar error' with `.envrc' files
;; the `peculiar error' problem has been solved by installing the `sh-mode'
;; reformatter `shfmt'
;; however this is still a useful function for easy setup of the environment
(defun create-envrc-and-update-local-environment ()
  "Create .envrc and update the local environment with `direnv'."
  (interactive)
  (let
      ((local-env
        (format "echo '%s' > .envrc"
                (read-from-minibuffer "Enter the local environment: "))))
    (shell-command local-env)
    (envrc-allow)))

(defun modify-envrc-and-update-local-environment ()
  "Modify/create .envrc and update the local environment with `direnv'."
  (interactive)
  (if (file-exists-p ".envrc")
      (delete-file ".envrc"))
  (create-envrc-and-update-local-environment))

;; Auto format source code
(use-package format-all
  :config
  (setq format-all-show-errors 'errors)
  :hook
  (prog-mode . format-all-mode)
  (format-all-mode . format-all-ensure-formatter))

;; Syntax checking
(use-package flycheck
  :config
  (setq-default flycheck-highlighting-mode 'symbols)
  (setq-default flycheck-indication-mode 'left-fringe)
  :hook (after-init . global-flycheck-mode))

;; Spell check
(use-package ispell
  :config
  (setq ispell-dictionary "en_US")
  (setq ispell-personal-dictionary (locate-user-emacs-file "personal_dict")))
(use-package flyspell
  :hook
  (text-mode . flyspell-mode)
  (prog-mode . flyspell-prog-mode))

;; Remember where the cursor was in each file
(use-package saveplace
  :straight (:type built-in)
  :config
  (setq save-place-file (locate-user-emacs-file "saveplace"))
  (setq save-place-forget-unreadable-files t)
  :hook ((prog-mode text-mode) . save-place-mode))

;; Ridiculously useful extensions for Emacs
(use-package crux
  :config
  (setq save-abbrevs 'silently)
  (setq-default abbrev-mode t)
  (crux-with-region-or-buffer indent-region) ;; to easily indent a region/buffer
  (crux-with-region-or-buffer untabify) ;; save all files with spaces not tabs
  (crux-with-region-or-line comment-or-uncomment-region)
  (crux-with-region-or-point-to-eol kill-ring-save)
  :bind
  ("C-c o" . crux-open-with)
  ([remap move-beginning-of-line] . crux-move-beginning-of-line)
  ("C-k" . crux-smart-kill-line)
  ("C-c f" . crux-recentf-find-file)
  ("C-c F" . crux-recentf-find-directory)
  ("C-c k" . crux-kill-other-buffers)
  ("C-c n" . crux-cleanup-buffer-or-region)
  ("C-c s" . crux-create-scratch-buffer)
  ("C-c i" . crux-ispell-word-then-abbrev)
  ("C-c D" . crux-delete-file-and-buffer))


(provide 'init-editor)
;;; init-editor ends here
