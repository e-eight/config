;;; init.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/modules/init-email.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(use-package notmuch
  :commands (notmuch)
  :hook (notmuch-hello-mode . (lambda () (display-line-numbers-mode 0)))
  :config
  (setq notmuch-show-logo nil)
  (setq notmuch-column-control t)
  (setq notmuch-hello-thousands-separator "")
  (setq notmuch-hello-auto-refresh t)
  (setq notmuch-search-oldest-first nil)
  (setq notmuch-hello-recent-searches-max 20)
  (setq notmuch-saved-searches
        '((:name "inbox (jobs)"
                 :query "tag:inbox AND tag:jobs"
                 :sort-order newest-first)
          (:name "unread (jobs)"
                 :query "tag:unread AND tag:jobs"
                 :sort-order newest-first)
          (:name "sent (jobs)"
                 :query "tag:sent AND tag:jobs"
                 :sort-order newest-first)
          (:name "starred (jobs)"
                 :query "tag:starred AND tag:jobs"
                 :sort-order newest-first)
          (:name "drafts (jobs)"
                 :query "tag:draft AND tag:jobs"
                 :sort-order newest-first)))
  (setq notmuch-fcc-dirs
        '(("dssohampal@gmail.com" . "jobs/sent -inbox +sent -unread +jobs"))))

;; (use-package mu4e
;;   :defer 6
;;   :straight ( :host github
;;               :repo "djcb/mu"
;;               :branch "master"
;;               :files ("mu4e/*")
;;               :pre-build (("./autogen.sh") ("make")))
;;   :custom   (mu4e-mu-binary (expand-file-name "mu/mu"
;;                                               (straight--repos-dir "mu")))
;;   :config
;;   (setq mu4e-maildir (expand-file-name "~/.mail"))
;;   (setq mu4e-get-mail-command "mbsync -a")
;;   (setq mu4e-update-interval 900)
;;   (setq message-kill-buffer-on-exit t)
;;   (setq mu4e-contexts
;;         `(,(make-mu4e-context
;;             :name "jobs"
;;             :match-func (lambda (msg)
;;                           (when msg
;;                             (string-prefix-p "/jobs" (mu4e-message-field msg :maildir))))
;;             :vars '((mu4e-trash-folder . "/jobs/trash")
;;                     (mu4e-sent-folder . "/jobs/sent")
;;                     (mu4e-drafts-folder . "/jobs/drafts")))))
;;   (setq mu4e-sent-messages-behavior (lambda ()
;;                                       (if (string=
;;                                            (message-sendmail-envelope-from) "dssohampal@gmail.com")
;;                                           'delete 'sent)))
;;   (setq mu4e-change-filenames-when-moving t)
;;   (setq mu4e-view-show-images nil))

(use-package smtpmail
  :straight (:type built-in)
  :config
  (setq sendmail-program (executable-find "msmtp"))
  (setq send-mail-function 'sendmail-send-it)
  (setq message-kill-buffer-on-exit t)
  (setq mail-specify-envelope-from t)
  (setq message-sendmail-envelope-from 'header)
  (setq mail-envelope-from 'header))


(provide 'init-email)
;;; init-email ends here
