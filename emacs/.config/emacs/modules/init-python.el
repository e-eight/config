;;; init-python.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-python.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'init-editor)

;; Use `IPython' if possible
(use-package python
  :straight (:type built-in)
  :config
  (when (executable-find "ipython")
    (setq python-shell-interpreter "ipython"))
  ;; black
  (add-hook 'python-mode-hook
            (lambda ()
              (setq-local fill-column 88))))

;; Add `anaconda-mode' to enhance `python.el'
(use-package anaconda-mode
  :hook
  (python-mode . anaconda-mode)
  (python-mode . anaconda-eldoc-mode))

;; Tidy up imports
(use-package python-isort
  :init (if (not (executable-find "isort"))
            (shell-command "pip install isort"))
  :config (add-hook 'python-mode-hook
                    'python-isort-on-save-mode))

;; Create and initialize a python virtual environment
(defun create-python-venv ()
  "Create/modify and initialize a Python virtual environment with `direnv'.
Also install a few useful packages."
  (interactive)
  (progn
    (modify-envrc-and-update-local-environment)
    (shell-command "pip install pip-tools")
    (find-file "dev-requirements.in")
    (save-buffer "dev-requirements.in")
    (kill-buffer "dev-requirements.in")
    (find-file "requirements.in")))

;; Pip-compile
(defun pip-compile ()
  "Compile (dev-)requirements.in using `pip-compile'."
  (interactive)
  (progn
    (shell-command "pip-compile --verbose requirements.in")
    (if (file-exists-p "dev-requirements.in")
        (shell-command "pip-compile --verbose dev-requirements.in"))))

;; Pip-sync
(defun pip-sync ()
  "Install Python packages with `pip-sync'."
  (interactive)
  (progn
    (envrc-reload)
    (if (and (file-exists-p "dev-requirements.txt")
             (y-or-n-p "Install development packages? "))
        (shell-command "pip-sync requirements.txt dev-requirements.txt")
      (shell-command "pip-sync requirements.txt"))))

;; Open jupyter notebooks in Emacs
(use-package ein
  :defer t)

;; Major mode for pip requirements file
(use-package pip-requirements
  :defer t)


(provide 'init-python)
;;; init-python ends here
