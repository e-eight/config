;;; init-completion.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-completion.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Use orderless to enhance built-in completion
;; (read the github README more carefully)
(use-package orderless
  :init
  (setq orderless-default-styles '(orderless-prefixes
                                   orderless-strict-initialism
                                   orderless-regexp
                                   orderless-literal
                                   orderless-flex)))

;; Set up Selectrum as an alternative/extension of the minibuffer
(use-package selectrum
  :init
  (setq completion-styles '(orderless))
  :config
  (selectrum-mode +1)
  (setq orderless-skip-highlighting (lambda () selectrum-is-active))
  (setq selectrum-highlight-candidates-function #'orderless-highlight-matches))

;; Add annotations to minibuffer completions
(use-package marginalia
  :init
  (marginalia-mode)
  :config
  (setq marginalia-annotators
        '(marginalia-annotators-heavy
          marginalia-annotators-light)))

;; Contextual menu in the minibuffer
;; need to understand `embark' better first
;; (use-package embark
;;   :bind
;;   (("C-," . embark-act)
;;    ("C-h B" . embark-bindings))
;;   :init
;;   (setq prefix-help-command #'embark-prefix-help-command)
;;   :config
;;   (add-to-list 'display-buffer-alist
;;                '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
;;                  nil
;;                  (window-parameters (mode-line-format . none)))))

;; Enhance the default completion in region function (capf)
(use-package corfu
  :init (corfu-global-mode)
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  :bind (:map corfu-map
              ("TAB" . corfu-next)
              ([tab] . corfu-next)
              ("M-TAB" . corfu-previous)
              ([backtab] . corfu-previous)))

;; Use dabbrev with corfu
(use-package dabbrev
  :straight (:type built-in)
  :bind (("M-/" . dabbrev-expand)
         ("C-M-/" . dabbrev-completion)))

;; Useful configurations to play nice with Corfu
(use-package emacs
  :straight (:type built-in)
  :init
  ;; TAB cycle when there are a few candidates
  (setq completion-cycle-threshold 2)
  ;; Enable indentation + completion using the TAB key
  (setq tab-always-indent 'complete))

;; Key-chord completion hints
(use-package which-key
  :demand t
  :config
  (which-key-mode))


(provide 'init-completion)
;;; init-completion ends here
