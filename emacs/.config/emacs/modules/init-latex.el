;;; init-latex.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init-latex.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; AUCTeX
(use-package tex
  :straight auctex
  :hook (LaTeX-mode . turn-on-reftex)
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  ;; do not auto set the master file
  (setq-default TeX-master nil)
  ;; use hidden dirs for auctex files
  (setq TeX-auto-local ".auctex-auto")
  (setq TeX-style-local ".auctex-style")
  ;; enable synctex
  (setq TeX-source-correlate-mode t)
  (setq TeX-source-correlate-method 'synctex)
  ;; enable reftex for citation
  (setq reftex-plug-into-AUCTeX t)
  (setq bib-cite-use-reftex-view-crossref t)
  ;; proper inline math for LaTeX
  (setq TeX-electric-math t)
  (setq LaTeX-electric-left-right-brace t)
  ;; proper subscript and superscript
  (setq TeX-electric-sub-and-superscript t)
  ;; save before each compile
  (setq TeX-save-query nil)
  ;; auto-update pdf after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)
  ;; view generated PDF with `pdf-tools'
  (unless (assoc "PDF Tools" TeX-view-program-list-builtin)
    (add-to-list 'TeX-view-program-list-builtin
                 '("PDF Tools" TeX-pdf-tools-sync-view)))
  (add-to-list 'TeX-view-program-selection
               '(output-pdf "PDF Tools")))

;; CDLaTeX for fast insertion of environment templates and math stuff
(use-package cdlatex
  :after latex
  :hook
  (LaTeX-mode . cdlatex-mode)
  (org-mode . org-cdlatex-mode)
  :config (setq cdlatex-use-dollar-to-ensure-math nil)
  :bind (:map cdlatex-mode-map
              ("$" . nil)
              ("(" . nil)
              ("{" . nil)
              ("[" . nil)
              ("<" . nil)
              ("|" . nil)
              ("TAB" . nil)
              ("^" . nil)
              ("_" . nil)
              ([control enter] . nil)))

;; AUCTeX with latexmk
(use-package auctex-latexmk
  :after latex
  :init
  ;; pass the -pdf flag when TeX-PDF-mode is active
  (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  ;; set latexmk as the default
  (if (eq major-mode 'latex-mode)
      setq TeX-command-default "LatexMk")
  :config
  (auctex-latexmk-setup))


(provide 'init-latex)
;;; init-latex ends here
