;;; init.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/init.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Bootstrap straight.el
(defvar native-comp-deferred-compilation-deny-list ()) ;; temporary fix till I can update emacs/straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Always load newest byte code
(setq load-prefer-newer t)

;; Install and integrate use-package with straight.el
(setq straight-use-package-by-default t)
(straight-use-package 'use-package)

;; Version control and follow symlinks (for dotfiles)
(use-package vc
  :straight (:type built-in)
  :config (setq vc-follow-symlinks t))

;; Easily open init file
(bind-key "C-c I" (lambda () (interactive) (find-file user-init-file)))

;; Easily reload init file
(bind-key "C-c R" (lambda () (interactive) (load-file user-init-file)))1

;; Temporary disable some warnings due to straight-el upgrade
(setq warning-minimum-level :error)

;; Disable default modeline format
(setq mode-line-format nil)

;; Pin entry
(use-package pinentry)
(use-package epg-config
  :straight (:type built-in)
  :config
  (setq epg-gpg-program "gpg2")
  (setq epa-pinentry-mode 'loopback))
(pinentry-start)

;; Load modules
(defvar module-dir (locate-user-emacs-file "modules"))
(add-to-list 'load-path module-dir)

(require 'init-defaults)
(require 'init-ui)
(require 'init-utils)
(require 'init-completion)
(require 'init-editor)
(require 'init-snippets)
(require 'init-lsp)
(require 'init-python)
(require 'init-rust)
(require 'init-go)
(require 'init-R)
(require 'init-latex)
(require 'init-org)
;; (require 'init-email)

;; Display init time
(defun init-time()
  (message
   (concat "Loading finished in " (emacs-init-time)
           " with %d garbage collections.")
   gcs-done))
(add-hook 'after-init-hook 'init-time)

;; Start server
;; (use-package server
;;   :straight (:type built-in)
;;   :hook (after-init . server-start))
(add-hook 'after-init-hook #'server-start)


(provide 'init)
;;; init.el ends here
