;;; early-init.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Soham Pal
;;
;; Author: Soham Pal <https://github.com/e-eight>
;; Maintainer: Soham Pal <dssohampal@gmail.com>
;; Created: June 05, 2021
;; Modified: June 05, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/e-eight/dots/emacs/.config/emacs/early-init.el
;; Package-Requires: ((emacs "28.0.50"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Disable package.el since I am using straight.el
(setq package-enable-at-startup nil)

;; Disable frame resize to improve startup time
(setq frame-inhibit-implied-resize t)

;; Disable UI elements
(push '(menu-bar-lines . 0) default-frame-alist)   ;; remove menubar
(push '(tool-bar-lines . 0) default-frame-alist)   ;; remove toolbar
(push '(vertical-scroll-bars) default-frame-alist) ;; remove scrollbars

;; Make startup faster by reducing the frequency of garbage collection.
(setq gc-cons-threshold (* 50 1000 1000))
(setq gc-cons-percentage 0.6)

;; Another small optimization concerns on =file-name-handler-alist= : on every
;; .el and .elc file loaded during start up, it has to runs those regexps
;; against the filename ; setting it to ~nil~ and after initialization finished
;; put the value back make the initialization process quicker.
(defvar file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

;; - Reseting garbage collection and file-name-handler values.
(add-hook 'after-init-hook
          `(lambda ()
             (setq gc-cons-threshold (* 2 1000 1000)
                   gc-cons-percentage 0.1
                   file-name-handler-alist file-name-handler-alist-original)
             (garbage-collect)) t)


(provide 'early-init)
;;; early-init ends here
