;;-*-coding: utf-8;-*-
(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("align" "" org-latex-align-skeleton :count 4)
    ("align*" "" org-latex-align-star-skeleton :count 0)
    ("aligns" "" org-latex-align-star-skeleton :count 1)
    ("ic" "" org-inline-code-skeleton :count 1)
    ("im" "" org-inline-math-skeleton :count 6)
    ("inm" "" inline-math-skeleton :count 2)
    ("itl" "" org-italics-skeleton :count 1)
    ("jp" "" org-jupyter-property-skeleton :count 1)
   ))

